trigger AvailabilityTrigger on Availability__c (before insert, before update) {
	new AvailabilityHelper().setGmtTimes(Trigger.new);
}