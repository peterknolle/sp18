({
    doInit: function(component) {
        var apiName = component.get("v.accountLookupApiName");
        component.set("v.fieldsList", [apiName]);
    },
    
    recordUpdated: function(component, event, helper) {
        var changeType = event.getParams().changeType;

        if (changeType === "CHANGED" || changeType === "LOADED") { 
            var apiName = component.get("v.accountLookupApiName");
            var recordFields = component.get("v.sourceRecordFields");
            
            var accountId = recordFields[apiName];
            component.set("v.accountId", accountId);
        }
    },
    
    handleSuccess : function(component, event, helper) {
		
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been updated successfully."
        });
        toastEvent.fire();
	}
})