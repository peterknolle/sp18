({	
    fireNew : function(component) {
        var newEvt = component.getEvent("availabilityNew");
        newEvt.fire();
    },
    
	fireEdit : function(component, event) {
        var editEvt = component.getEvent("availabilityEdit");
       
        console.log('list handling event', event);
        editEvt.setParams({
            availabilityId : event.getParam("availabilityId")
        });
        
        editEvt.fire();
    }
})