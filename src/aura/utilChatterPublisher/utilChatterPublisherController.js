({
    /*
    recordUpdated: function(component, event, helper) {
       
        var changeType = event.getParams().changeType;
        
        console.log('changeType=' + changeType);
        
		if (changeType === "LOADED") { 
        	console.log("\nloaded the record");
            var wh = component.get("v.newTask.What");
            console.log("wh.Id=" + wh.Id);
            component.set("v.newTask.WhatId", wh.Id);
            console.log(wh);
        }
        
    },
*/    
    handleRecordIdChange: function(component, event, helper) {
        console.log("handleRecordIdChange: v.recordId = " + component.get("v.recordId"));
        
        var recId = event.getParam("value");
		component.set("v.newTask.WhatId", recId);
        component.find("newTaskWhatRecord").reloadRecord(true, function(res) {
			console.log('back from reload');
            console.log(component.get("v.newTask.What"));
            var cn = component.get("v.newTask.What.CaseNumber");
            console.log(cn);
            if (cn) {
            	var what = component.get("v.newTask.What");
                what['Name'] = cn;
                component.set("v.relatedToName", cn);
            } else {
                component.set("v.relatedToName", component.get("v.newTask.What.Name"));
            }
            console.log(component.get("v.newTask.What"));
        });
        
        console.log("done with change 04");
    },

    handleSaveTask: function(component, event, helper) {
        console.log(component.get("v.newTask"));
        console.log(component.get("v.newTask.What"));
        // save it...
        /*
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // record is saved successfully
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Saved",
                        "message": "The record was saved."
                    });
                    resultsToast.fire();

                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    console.log('Problem saving contact, error: ' + 
                                 JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + 
                                ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
        */
    }
})