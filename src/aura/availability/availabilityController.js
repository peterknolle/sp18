({
    doInit : function(component, event, helper) {
        helper.loadDayOptions(component);
        helper.loadTimeZoneOptions(component);
        helper.loadTypeOptions(component);
    },
    
    handleAfterScriptsLoaded : function(component, event, helper) {
        j$ = jQuery.noConflict(); 
    },
    
    save : function(component, event, helper) {
        helper.save(component, false);
    },
    
    saveAndAddMore : function(component, event, helper) {
        helper.save(component, true);
    }

})