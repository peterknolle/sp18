({
    loadTimeZoneOptions : function(component) {
        this.getValues(component, "c.getTimeZoneOptions", "v.timeZoneOptions");
    },
    
    loadDayOptions : function(component) {
        this.getValues(component, "c.getDayOptions", "v.dayOptions");
    },
    
    loadTypeOptions : function(component) {
        this.getValues(component, "c.getTypeOptions", "v.typeOptions");
    },
    
    getValues : function(component, actionName, attributeName) {
        var action = component.get(actionName);
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set(attributeName, a.getReturnValue());
            }
            // else: TODO
        });
        $A.enqueueAction(action);
    },
    
    save : function(component, addMore) {
        if (this.validate(component)) {
            try {
                this.disableActions(component);
                this.saveAvailability(component, addMore);
            } catch (e) {
                this.enableActions(component);
            }
        } else {
            this.showError(component);
        }
    },
    
    validate : function(component) {
        var isValid = true;
        var startTime = component.find("startTime");
        var endTime = component.find("endTime");
        
        startTime.set("v.errors", null);
        endTime.set("v.errors", null);
        
        if (this.validateRequired(startTime)) {
            isValid = this.validateTime(startTime) && isValid;
        } else {
            isValid = false;
        }
        
        if (this.validateRequired(endTime)) {
            isValid = this.validateTime(endTime) && isValid;
        } else {
            isValid = false;
        }
        
        return isValid;
    },
    
    validateTime : function(timeComponent) {
        var valid = true;
        
        try {
            var d = Date.parse(timeComponent.get("v.value"));
            valid = !isNaN(d);
        } catch (e) {
            valid = false;
        }
        
        if (!valid) {
            this.addError(timeComponent, "Invalid Time");
        }
        
        return valid;
    },
    
    validateRequired : function(requiredComponent) {
        var valid = true;
        var val = requiredComponent.get("v.value");
        console.log('required value', val);
        console.log('requiredComponent', requiredComponent);
        
        valid = !$A.util.isEmpty(val);
        if (!valid) {
            this.addError(requiredComponent, "Required");
        }
        
        return valid;
    },
    
    addError : function(component, msg) {
        var errors = component.get("v.errors");
        if (errors === null) {
            errors = [];
        }
        errors.push({message: msg});
        
        component.set("v.errors", errors);
    },
    
    disableActions : function(component) {
        this.disableButton( component.find("save") );
        this.disableButton( component.find("saveAndAddMore") );
    },
    
    disableButton : function(btn) {
        btn.set("v.disabled", true);
        btn.set("v.label", "Saving...");
    },
    
    enableActions : function(component) {
        this.enableButton( component.find("save"), "Save" );
        this.enableButton( component.find("saveAndAddMore"), "Save and Add More" );
    },
    
    enableButton : function(btn, label) {
        btn.set("v.disabled", false);
        btn.set("v.label", label);
    },
    
    saveAvailability : function(component, addMore) {
        var action = component.get("c.saveAvailability");
        var availability = component.get("v.availability");
        
        action.setParams({
            availability : availability
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            self.enableActions(component);
            
            if (a.getState() === "SUCCESS") {
                var savedAvailability = a.getReturnValue();
                component.set("v.availability", savedAvailability);
                self.fireSaved(component, savedAvailability);
                
                if (addMore) {
                    component.set("v.availability.Id", null);
                    self.setCurrentAvailabilityToDefault(component);
                    self.showSuccess(component, true);
                } else {
                    self.showSuccess(component, false);
                }
            } else {
                self.showError(component);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    fireSaved : function(component, savedAvailability) {
        var savedEvt = $A.get("e.c:availabilitySaved");  
        
        console.log('fireSaved', component.get("v.sObjectName"));
        savedEvt.setParams({
            availability : savedAvailability,
            sObjectName :  component.get("v.sObjectName")
        });
        savedEvt.fire();
    },
    
    showSuccess : function(component, fadeOut) {
        $A.util.addClass(component.find("errorMsg"), "toggle");
        var successElem = j$( component.find("successMsg").getElement() ).eq(0);
        
        successElem.show();
        if (!$A.util.isEmpty(fadeOut) && fadeOut) {
            successElem.fadeOut(2000);
        }
    },
    
    showError : function(component, msg) {
        $A.util.addClass(component.find("successMsg"), "toggle");
        $A.util.removeClass(component.find("errorMsg"), "toggle");
    },
    
    setCurrentAvailabilityToDefault : function(component) {
        var def = component.get("v.defaultAvailability");
        if (!$A.util.isEmpty(def)) {
            var curr = component.get("v.availability");
            curr = j$.extend(true, {}, def);
            component.set("v.availability", curr);
        }
        // else: TODO
    }
})