({
	doInit : function(component, event, helper) {
        console.log(event.getParam("context"));
        helper.setDateTime(component, new Date());
	},
    
    refresh : function(component, event, helper) {
        console.log('refresh');
        helper.setDateTime(component, new Date());
    }
})