({  
    applyInfoChange : function(component) {
    	var info = component.get("v.info");
        if (!$A.util.isEmpty(info)) {
            var title = info.isAvailable ? "Currently Available" : "Next Availability";
            component.find("card").set("v.title", title);
        }
	},
    		
    fireNew : function(component) {
        var newEvt = component.getEvent("availabilityNew");
        newEvt.fire();
    },
    
    fireEdit : function(component, event) {
        var editEvt = component.getEvent("availabilityEdit");
        editEvt.setParams({
            availabilityId : event.getParam("availabilityId")
        });
        
        editEvt.fire();
    }
})