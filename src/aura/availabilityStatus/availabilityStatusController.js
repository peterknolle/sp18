({
    handleInfoChange : function(component, event, helper) {
        helper.applyInfoChange(component);
    },
    
	handleAvailabilityNew : function(component, event, helper) {
		helper.fireNew(component, event);
	},
    
	handleAvailabilityEdit : function(component, event, helper) {
		helper.fireEdit(component, event);
	}
})