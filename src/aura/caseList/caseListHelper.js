({
	getItems : function(component, offset, limit) {
        debugger;
        this.getOpps(component);
		var action = component.get("c.getItemInfo"); 
 		
        action.setParams({
            offsetVal : offset,
            limitVal : limit
        });
        
        $A.logger.subscribe("INFO", function(level, message, error) {
        	console.log(error);
        });
        
        action.setCallback(this, function(a) {
            var info = a.getReturnValue();
            console.log(info);
            if (a.getState() === "SUCCESS") {
                component.set("v.items", info.items);
                component.set("v.numTotalItems", info.numTotalItems);
            } else if (a.getState() === "ERROR") {
                $A.logger.log("ERROR", "got an error on the callback", a.getError());
            }
        });
            
        $A.enqueueAction(action); 
	},
    
    getOpps : function(component) {
        debugger;
        var action = component.get("c.getOppInfos"); 
        action.setParams({
            StageName: component.get("v.stageName")
        });

        action.setCallback(this, function(a) {
            var infos = a.getReturnValue();
            component.set("v.oppInfos", infos);
        });   
        
        $A.enqueueAction(action); 
    }
})