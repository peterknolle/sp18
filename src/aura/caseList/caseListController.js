({
	doInit : function(component, event, helper) {
        helper.getItems(component, 0, component.get("v.pageSize"));
	},
    
    handlePage : function(component, event, helper) {
    	var offset = event.getParam("offset");
        var limit = event.getParam("limit");
        
        helper.getItems(component, offset, limit);
	}
})