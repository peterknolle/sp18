({
	fireNew : function(component) {
        var newEvt = component.getEvent("availabilityNew");
        newEvt.fire();
    },
    
    fireEdit : function(component) {
        var editEvt = component.getEvent("availabilityEdit");
       	editEvt.setParams({
            availabilityId : component.get("v.info.availability.Id"),
            sObjectName : component.get("v.sObjectName")
        });
        
        editEvt.fire();
    },
    
    deleteAvailability : function(component) {
        var action = component.get("c.deleteAvailability");
        var info = component.get("v.info");
        
        action.setParams({
            availabilityId : info.availability.Id
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var deletedEvt = $A.get("e.c:availabilityDeleted");
                deletedEvt.setParams({
                    availability: info.availability,
            		sObjectName : component.get("v.sObjectName")
                });
                deletedEvt.fire();
            } // else TODO
        });
        
        $A.enqueueAction(action);
    }
})