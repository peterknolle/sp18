({
	handleNewClicked : function(component, event, helper) {
		helper.fireNew(component);
	},
    
	handleEditClicked : function(component, event, helper) {
		helper.fireEdit(component);
	},
    
	handleDeleteClicked : function(component, event, helper) {
		helper.deleteAvailability(component);
	}
})