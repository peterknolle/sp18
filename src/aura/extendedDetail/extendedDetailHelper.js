({
    
    onInit : function(component) {  
        console.log('sub helper');
        console.log(component.getDef().getHelper());        
//        console.log(component.getConcreteComponent().getDef().getHelper().loadRecord(component));
 //       component.getSuper().getHelper().onInit(component);
        console.log('extended helper');
    },
        
    loadRecord : function(component) {
        var stdFields = component.get("v.standardFields");
        var extFields = component.get("v.fields");
        var fields = stdFields + (extFields != null ? ','+ extFields : "");
        
        var action = component.get("c.getObject"); 
        action.setParams({
            recordId: component.get("v.recordId"),
            objectType: component.get("v.objectType"),
            fields: fields
        });
 
        action.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.record", a.getReturnValue());
        });
            
        $A.enqueueAction(action); 
	},
    
    postGetHook : function(component) {
        console.log('sub post hook');
        console.log(component);
    },
    
    doSomething : function(component) {
        console.log('do somehting');
    }
})