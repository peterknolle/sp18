({
    callServer: function(component, action, successHandlerCb, errorHandlerCb) {
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                successHandlerCb(response);
            }
            else if (state === "ERROR") {
                if (typeof errorHandler === "function") {
                    errorHandlerCb(response);
                } else {
                    this.handleErrorDefault(component, response);
                }
            }
        });
        $A.enqueueAction(action);
    },

    handleErrorDefault: function(component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
})
