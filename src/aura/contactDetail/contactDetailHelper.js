({
    loadContact : function(component) {
        var action = component.get("c.getContact");
        
        action.setParams({
            contactId : component.get("v.contactId")
        });
        
        action.setCallback(this, function(a) {
            console.log(a == action);
            if (a.getState() === "SUCCESS") {
                component.set("v.contact", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getErrors());
                component.set("v.errorMsg", "Error occurred");
            }
        });
        console.log(action)
        $A.enqueueAction(action);
    },
    
    updateContact : function(component) {
        var action = component.get("c.saveContact");
        
        action.setParams({
            con : component.get("v.contact")
        });
        
        action.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.contact", a.getReturnValue());
        });
        
        $A.enqueueAction(action);
    }
})