({
	openSubtab : function(component, event) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getEnclosingTabId().then(function(tabId) {
            console.log(tabId);
            workspaceAPI.openSubtab({
                parentTabId: tabId,
                url: '/apex/subtabTest?id=' + component.get("v.recordId"),
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    
    openTabWithSubtab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            url: '#/sObject/' + component.get("v.recordId") + '/view',
            focus: true
        }).then(function(parentTabId) {
            workspaceAPI.openSubtab({
                parentTabId: parentTabId,
                url: '/apex/subtabTest?id=' + component.get("v.recordId"),
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})