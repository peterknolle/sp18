({
    LEVEL_ACCOUNT: 2,
    LEVEL_CONTACT: 1,

    initialize : function(component) {
        var action = component.get("c.initialize");
        action.setParams({ myObjId: null });

        this.callServer(component, action, function(response) {
            var returnVal = response.getReturnValue();

            component.set("v.myObject", returnVal.myObject);
            component.set("v.availableUsers", returnVal.availableUsers);
            component.set("v.availableAccounts", returnVal.availableAccounts);
        });
    },

    handleAccountChange : function(component, event) {
        var selectedAccountId = component.get("v.myObject.Account__c");

        this.reset(component, this.LEVEL_ACCOUNT);

        if (selectedAccountId !== "") {
            var action = component.get("c.getAvailableContacts");
            action.setParams({accountId: selectedAccountId});

            this.callServer(component, action, function(response) {
                var returnVal = response.getReturnValue();
                component.set("v.availableContacts", returnVal);
                component.set("v.contactDisabled", false);
            });
        }
    },

    handleContactChange : function(component, event) {
        var selectedContactId = component.get("v.myObject.Contact__c");

        this.reset(component, this.LEVEL_CONTACT);

        if (selectedContactId !== "") {
            var action = component.get("c.getAvailableOpportunites");
            action.setParams({contactId: selectedContactId});

            this.callServer(component, action, function(response) {
                var returnVal = response.getReturnValue();
                component.set("v.availableOpportunities", returnVal);
                component.set("v.opportunityDisabled", false);
            });
        }
    },

    reset : function(component, level) {
        if (level >= this.LEVEL_ACCOUNT) {
            component.set("v.availableContacts", []);
            component.set("v.contactDisabled", true);
        }
        if (level >= this.LEVEL_CONTACT) {
            component.set("v.availableOpportunities", []);
            component.set("v.opportunityDisabled", true);
        }
    }
})
