({
    doInit : function(component, event, helper) {
       helper.initialize(component);
   },

   handleAccountChange : function(component, event, helper) {
       helper.handleAccountChange(component, event);
   },

   handleContactChange : function(component, event, helper) {
       helper.handleContactChange(component, event);
   },

})
