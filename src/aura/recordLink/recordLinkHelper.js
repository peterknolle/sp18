({
	navigateToRecord : function(recId) {
		var navEvent = $A.get("e.force:navigateToSObject");

    	navEvent.setParams({
      		recordId: recId,
            slideDevName: "detail"
    	});
        
    	navEvent.fire(); 
	}
})