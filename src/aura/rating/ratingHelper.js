({
    isRatingJsLoaded: function(component) {
    	// check in case coming from afterRender,
        // before scripts are loaded
        var ready = component.get("v.ready");
        console.log(ready);
        return ready === true;
	},
 
	createRating : function(component) {
        if (!this.isRatingJsLoaded(component)) {
       		return;   
		}

		var ratingElem = component.find("rating").getElement();
        
		$(ratingElem).raty({
        	starType: "i",
            number: component.get("v.numberOfStars"),            
            score: component.get("v.value"),
            click: function(score, evt) {
                component.set("v.value", score);
            }
        });
	},
    
    loadRating : function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.loadRating");
        
        action.setParams({ 
            recordId: recordId 
        });
        action.setAbortable(true);
        
        action.setCallback(this, function(a){
            var status = a.getState();
            console.log(status);
            if (status === "SUCCESS") {
                var retVal = a.getReturnValue();
                component.set("v.value", retVal);
                if (this.isRatingJsLoaded(component)) {
                    var ratingElem = component.find("rating").getElement();
                    $(ratingElem).raty("score", retVal);
                }
            } else if (status == "ERROR") {
                component.set("v.errorMessage", status);
                component.set("v.showError", true);
                console.log(a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveRating : function(component) {
        var recordId = component.get("v.recordId");
        var rating = component.get("v.value");
        var action = component.get("c.saveRating");
        
        action.setParams({
            recordId: recordId, 
            rating: rating
        });
        
        action.setAbortable(true);
        
        action.setCallback(this, function(a){
            var status = a.getState();
            if (status == "ERROR") {
                component.set("v.errorMessage", status);
                component.set("v.showError", true);
                console.log(a.getError());
            }
        });
        $A.enqueueAction(action);
    }
})