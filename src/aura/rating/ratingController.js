({
	afterScriptsLoaded : function(component, event, helper) {
        component.set("v.ready", true);
        helper.createRating(component);
	},
    
    doInit : function(component, event, helper) {
        helper.loadRating(component);
    },
    
    handleRatingChange : function(component, event, helper) {
        helper.saveRating(component);
    }
})