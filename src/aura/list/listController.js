({
	doInit : function(component, event, helper) {
		helper.loadList(component);  
        component.set("v.sObjectInfo", {
            Label: "Account",
            LabelPlural: "Accounts"
        })
	},
    
    waiting: function(component, event, helper) {
        // standard waiting / done waiting w/ spinner as documented
        var spinner = component.find("spinner");
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();
    },
    
    doneWaiting: function(component, event, helper) {
    	var spinner = component.find("spinner");
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();
    }
})