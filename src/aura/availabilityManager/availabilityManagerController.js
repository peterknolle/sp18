({
    doInit : function(component, event, helper) {
        helper.loadDefaultAvailability(component);
        helper.getAvailabilities(component);
    },
    
    handleAfterScriptsLoaded : function(component, event, helper) {
        j$ = jQuery.noConflict(); 
    },
    
    /* COMPONENT LEVEL EVENT HANDLER */
    handleAvailabilityNew : function(component, event, helper) {
        helper.setCurrentAvailabilityToDefault(component);
        helper.moveFocusToForm(component);
    },
    
    /* COMPONENT LEVEL EVENT HANDLER */
    handleAvailabilityEdit : function(component, event, helper) {
        var availabilityId = event.getParam("availabilityId");
        component.set("v.availabilityId", availabilityId);
        
        helper.loadAvailability(component);
    },
    
    /* APPLICATION LEVEL EVENT HANDLER */
    handleAvailabilitySaved : function(component, event, helper) {
        if (helper.isCorrectSObjectName(component, event)) {
        	helper.getAvailabilities(component);
        }
    },
    
    /* APPLICATION LEVEL EVENT HANDLER */
    handleAvailabilityDeleted : function(component, event, helper) {
        if (helper.isCorrectSObjectName(component, event)) {
        	helper.getAvailabilities(component);
            helper.removeEditAvailabilityIfDeleted(component, event);
        }
    }
})