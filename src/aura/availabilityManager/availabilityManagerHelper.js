({
    isCorrectSObjectName : function(component, event) {
    	var componentSObjectName = component.get("v.sObjectName");
        var eventSObjectName = event.getParam("sObjectName");
        
        return componentSObjectName.toLowerCase() === eventSObjectName.toLowerCase();
    },
    
    removeEditAvailabilityIfDeleted : function(component, event) {
        var eventAvailId = event.getParam("availability").Id;
        var componentAvailId = component.get("v.availabilityId");
                
        if ( !$A.util.isEmpty(componentAvailId) && (eventAvailId === componentAvailId) ) {
            this.setCurrentAvailabilityToDefault(component);
            component.set("v.availabilityId", null);
        }
    },
    
    getAvailabilities : function(component) {
        var action = component.get("c.getAvailabilities");
        
        action.setParams({
            recordId : component.get("v.recordId"),
            sObjectName : component.get("v.sObjectName")
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var infos = a.getReturnValue();
                self.updateComponents(component, infos);
            }
            // else: TODO
            
        });
        $A.enqueueAction(action);
    },  
    
    updateComponents : function(component, infos) {
        this.updateStatus(component, infos);
        if (infos.length > 0) {
            // remove the first since that's already displayed in the status component
            component.set("v.infos", infos.slice(1, infos.length) );
        } else {
            component.set("v.infos", []);
            component.set("v.availabilityId", null);
            this.setCurrentAvailabilityToDefault(component);
        }
    },
    
    updateStatus : function(component, infos) {
        var statusComponent = component.find("availabilityStatus");
        if (!$A.util.isEmpty(infos)) {
            statusComponent.set("v.info", infos[0]);
        } else {
            statusComponent.set("v.info", null);
        }
        statusComponent.set("v.loaded", true);
    },
    
    loadAvailability : function(component) {
        var action = component.get("c.getAvailability");
        action.setParams({
            availabilityId: component.get("v.availabilityId")
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var availability = a.getReturnValue();
                component.set("v.availability", availability);
                self.moveFocusToForm(component);
            }
            // else: TODO
        });
        $A.enqueueAction(action);
    },

    loadDefaultAvailability : function(component) {
        var action = component.get("c.getDefaultAvailability");
        action.setParams({
            recordId : component.get("v.recordId"),
            sObjectName : component.get("v.sObjectName") 
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var def = a.getReturnValue();
                
                component.set("v.defaultAvailability", def);
                console.log('got default availability from server', def);
                self.setCurrentAvailabilityToDefault(component);
            }
            // else: TODO
        });
        $A.enqueueAction(action);
    },
    
    setCurrentAvailabilityToDefault : function(component) {
        var def = component.get("v.defaultAvailability");
        if (!$A.util.isEmpty(def)) {
            var curr = component.get("v.availability");
            curr = j$.extend(true, {}, def);
            component.set("v.availability", curr);
        }
        // else: TODO
    },
    
    moveFocusToForm : function(component) {
        j$(component.getElement())
        	.find("div.cAvailability").get(0).scrollIntoView(false);
        
        j$(component.getElement())
        	.find("input.slds-datepicker--time")
        	.eq(1)
        	.focus();
    }
})