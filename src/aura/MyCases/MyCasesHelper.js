({
	getItems : function(component, offset, limit) {
		var action = component.get("c.getItemInfo"); 
 		
        action.setParams({
            offsetVal : offset,
            limitVal : limit
        });

        action.setCallback(this, function(a) {
            var info = a.getReturnValue();
            console.log('here i am');
            console.log(info);
            if (a.getState() === "SUCCESS") {
                component.set("v.items", info.items);
                component.set("v.numTotalItems", info.numTotalItems);
            } else if (a.getState() === "ERROR") {
                console.log(a.getError());
            }
        });
            
        $A.enqueueAction(action); 
	}
})