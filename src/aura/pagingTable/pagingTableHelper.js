({
	requestPage : function(component, newPageNumber) {
        // subtract 1 since page numbers are 1 based and items/soql offset is 0 based
        var offset = (newPageNumber - 1) * component.get("v.pageSize");

		var pageEvent = component.getEvent("page");
        pageEvent.setParams({
            offset : offset,
            limit : component.get("v.pageSize")
        }).fire();

        component.set("v.pageNumber", newPageNumber);
        this.setHasControls(component);
	},
     
    handleQtyChange : function(component) {
        var resultSize = component.get("v.resultSize"); 
        var pageSize = component.get("v.pageSize");
        
        if (resultSize && pageSize != 0 && resultSize) {
            component.set("v.totalPages", Math.ceil(resultSize / pageSize));
        }
        this.setHasControls(component);
    },
    
    setHasControls : function(component) {
        var resultSize = component.get("v.resultSize"); 
        var pageSize = component.get("v.pageSize");
        var pageNumber = component.get("v.pageNumber");
        
        component.set("v.hasNext", pageNumber * pageSize < resultSize);
        component.set("v.hasPrevious", pageNumber > 1)
    }
})