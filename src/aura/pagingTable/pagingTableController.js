({
    
    first : function(component, event, helper) { 
        helper.requestPage(component, 1);
    },
    
    previous : function(component, event, helper) {
        var newPageNumber = component.get("v.pageNumber") - 1;
        helper.requestPage(component, newPageNumber);
    },
    
    next : function(component, event, helper) {
        var newPageNumber = component.get("v.pageNumber") + 1;
        helper.requestPage(component, newPageNumber);
    },
    
    last : function(component, event, helper) {
        var offset = component.get("v.resultSize") - component.get("v.pageSize");
        helper.requestPage(component, component.get("v.totalPages"));
    },
    
    doInit : function(component, event, helper) {
        helper.handleQtyChange(component);
    },
    
    handleQtyChange : function(component, event, helper) {
        helper.handleQtyChange(component); 
    }
})