public with sharing class DescribeController {

    public Graph theGraph { get; set; }
    public List<SelectOption> nodeOptions { get; set; }
    
    public String sourceNodeStr { get; set; }
    public String destNodeStr { get; set; }
    
    public transient List<GraphPath> paths { get; set; }
    public transient String json { get; set; }
    
    public DescribeController() {
        paths = new List<GraphPath>();
        buildGraph();
        buildSelectOptions();
    }
    
    public void buildGraph() {
        theGraph = new Graph();
        
        for (Schema.SObjectType objType : Schema.getGlobalDescribe().values()) {
            // Add parent Graph.Node
            Schema.DescribeSObjectResult objDesc = objType.getDescribe();
            Graph.Node parentNode = new Graph.Node(objDesc.getName());
            theGraph.addNode(parentNode);
            
            // Add Graph.Edges from all children to their parent
            for (Schema.ChildRelationship child : objDesc.getChildRelationShips()) {
                Schema.DescribeSObjectResult childDesc = child.getChildSObject().getDescribe();
                
                Graph.Node childNode = new Graph.Node(childDesc.getName());
                theGraph.addEdge(childNode, parentNode);
            }
        }
    }
    
    private void buildSelectOptions() {
        nodeOptions = new List<SelectOption>();
        for (Graph.Node n : theGraph.getNodes()) {
            nodeOptions.add( new SelectOption(n.value, n.value) );
        }
        
        nodeOptions.sort();
    }  
    
    public PageReference generatePaths() {
        GraphSearcher searcher = new GraphSearcher(theGraph);
        paths = searcher.findPaths( new Graph.Node(sourceNodeStr), new Graph.Node(destNodeStr) );
  
        GraphFormatter formatter = new GraphFormatter(theGraph);
        json = formatter.getJson(paths);
        
        return null;
    }
}