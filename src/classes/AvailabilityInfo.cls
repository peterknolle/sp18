public class AvailabilityInfo {
    @AuraEnabled
    public Availability__c availability { get; set; }
    
    @AuraEnabled
    public Boolean isAvailable { get; set; }
    
    public AvailabilityInfo(Availability__c a, Boolean isa) {
        this.availability = a;
        this.isAvailable = isa;
    }
    
}