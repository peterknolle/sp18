@RestResource(urlMapping='/v1.0/messages')
global class MessageService {

    private static final String GROUP_ID = '0F9B00000000Jue';
    
    @HttpGet
    global static void getMessages() {
        
        // buildMessages gets FeedItems
        List<Message> messages = buildMessages();
       
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Origin','https://pjkgus-developer-edition.gus.force.com');
        res.responseBody = Blob.valueOf( JSON.serialize(messages) );
        
    }
    
    private static List<Message> buildMessages() {
        List<Message> messages = new List<Message>();
        
        for (FeedItem item : [
            SELECT Title, Body, CreatedDate
            FROM FeedItem
            WHERE ParentId = :GROUP_ID
            ORDER BY CreatedDate DESC
            Limit 5
        ]) {
            messages.add(new Message(item.CreatedDate.date(), item.Title, item.Body));
        }
        return messages;
    }
    
    class Message { 
        public Message(Date d, String t, String b) {
            messageDate = d;
            title = t;
            body = b;
        }
        
        public Date messageDate { get; set; }
        public String title { get; set; }
        public String body { get; set; }
    }
}