public class Graph {
    private Map<Node, Set<Node>> adjacenyList;
        
    public Graph() {
        adjacenyList = new Map<Node, Set<Node>>();
    }
        
    public void addNode(Node n) {
        if ( !adjacenyList.keySet().contains(n) ) {
            adjacenyList.put( n, new Set<Node>() );
        }
    }
        
    public void addEdge(Node sourceNode, Node destNode) {
        addNode(sourceNode);
        addNode(destNode);
        getAdjacentNodes(sourceNode).add(destNode);
    }
        
    public Set<Node> getAdjacentNodes(Node sourceNode) {
        Set<Node> adjNodes = adjacenyList.get(sourceNode);
        if (adjNodes == null) {
            adjNodes = new Set<Node>();
            adjacenyList.put(sourceNode, adjNodes);
        }
        return adjNodes;
    }
        
    public Set<Node> getNodes() {
        return new Set<Node>(adjacenyList.keySet());
    }   
    
    public class Node {
        // Using String for 'value' for simplicity. Could use
        // Object or Comparable to be more flexible.
        public String value { get; set; }
        
        public Node(String val) {
            value = val;
        }
        
        public Boolean equals(Object o) {
           Node other = (Node) o;
           if (other == null) {
               return false;
           }
           return this.value.equals(other.value);
        }
        
        public Integer hashCode() {
            return value.hashCode();
        }
        
        public Integer compareTo(Object o) {
            Node other = (Node) o;
            if (other == null) {
                return 1;
            }
            return this.value.compareTo(other.value);
        }
    }
    
}