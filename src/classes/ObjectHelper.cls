public with sharing class ObjectHelper {
    
    public List<Option> getOptions(sObjectField field) {
        Schema.DescribeFieldResult fieldDesc = field.getdescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldDesc.getPicklistValues();

        List<Option> opts = new List<Option>();
        for(Schema.PicklistEntry pe : picklistEntries) {
            opts.add( new Option(pe.getLabel(), pe.getValue()) );
        }
        
        return opts;
    }
    
    public Set<String> getPicklistValues(sObjectField field) {
        return this.getPicklistValues(field.getDescribe().getPicklistValues());    
    }
    
    private Set<String> getPicklistValues(List<Schema.PicklistEntry> picklistEntries) {
        Set<String> values = new Set<String>();

        for (PicklistEntry pe : picklistEntries) {
            values.add(pe.getValue());
        }
        
        return values;
    }
    
    public void filterOptions(List<Option> opts, Set<String> optionsToRetain) {
        for (Integer i = opts.size() - 1; i <= 0; i--) {
            Option opt = opts.get(i);
            if ( !optionsToRetain.contains(opt.value) ) {
                opts.remove(i);
            }
        }
    }
    
    public Map<String, String> getFieldNames(List<String> sObjectNames) {
        Map<String, String> objNameToFieldName = new Map<String, String>();
        for (String son : sObjectNames) {
            String fieldName = son;
            if (!fieldName.endsWith('__c')) {
                fieldName += '__c';
            }
            
            objNameToFieldName.put(son, fieldName);
        }
        
        return objNameToFieldName;
    }
}