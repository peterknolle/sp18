public class GraphPath {
    private List<Graph.Node> path { get; set; }
        
    // Need both Set and List since no concept
    // of an Ordered Set (e.g., LinkedSet).
    // Allows constant time inclusion check (i.e., map.get()).
    private Set<Graph.Node> pathSet;
        
    public GraphPath() {
        path = new List<Graph.Node>();
        pathSet = new Set<Graph.Node>();
    }
        
    public GraphPath(GraphPath orig) {
        this();
        for (Graph.Node n : orig.getNodes()) {
            add(n);
        }
    }
        
    public void add(Graph.Node n) {
        path.add(n);
        pathSet.add(n);
    }
        
    public Graph.Node removeLast() {
        Graph.Node n = path.remove(path.size() - 1);
        pathSet.remove(n);
        
        return n;
    }
        
    public Graph.Node remove(Integer pathNum) {
        Graph.Node n = path.remove(pathNum);
        pathSet.remove(n);
        return n;
    }
        
    public Integer getLength() {
        return path.size();
    }
        
    public Boolean contains(Graph.Node n) {
        return pathSet.contains(n);
    }
        
    public Graph.Node get(Integer pathNum) {
        return path.get(pathNum);
    }
        
    public List<Graph.Node> getNodes() {
        return new List<Graph.Node>(path);
    }
}