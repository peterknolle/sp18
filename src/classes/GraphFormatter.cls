public class GraphFormatter {
    private final Graph g;
        
    public GraphFormatter(Graph gr) {
        g = gr;
    }
        
    public String getJson(List<GraphPath> paths) {
        JSONGenerator gen = JSON.createGenerator(false);
        
        gen.writeStartObject();
            
        // Nodes
        Set<Object> nodes = new Set<Object>();
        Set<Object> edges = new Set<Object>();

        for (GraphPath path : paths) {
            for (Integer i = 1; i < path.getLength(); i++) {
                Object sourceVal = path.get(i - 1).value;
                Object destVal = path.get(i).value;
                nodes.add(sourceVal);
                nodes.add(destVal);
                      
                edges.add(new List<Object>{sourceVal, destVal});
            }
        }    
            
        gen.writeObjectField('nodes', nodes);
        gen.writeObjectField('edges', edges);
           
        return gen.getAsString();
    } 
}