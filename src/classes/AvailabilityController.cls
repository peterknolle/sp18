public class AvailabilityController {
    
    @AuraEnabled
    public static List<Option> getTimeZoneOptions() {
        ObjectHelper objectHelp = new ObjectHelper();
        
        List<Option> optsWithFullLabels = objectHelp.getOptions(User.TimeZoneSidKey);
        Set<String> timeZonePicklistVals = objectHelp.getPicklistValues(Availability__c.Time_Zone__c);
        objectHelp.filterOptions(optsWithFullLabels, timeZonePicklistVals);
        
        return optsWithFullLabels;
    }
    
    @AuraEnabled
    public static List<Option> getDayOptions() {
        return new ObjectHelper().getOptions(Availability__c.Day__c);
    }
    
    @AuraEnabled
    public static List<Option> getTypeOptions() {
        return new ObjectHelper().getOptions(Availability__c.Type__c);
    }
    
    @AuraEnabled
    public static Availability__c saveAvailability(Availability__c availability) {
        System.debug(LoggingLevel.INFO, 'saving...' + availability);
        upsert availability;
        return availability;
    }
    
    @AuraEnabled
    public static void deleteAvailability(Id availabilityId) {
        System.debug(LoggingLevel.INFO, 'deleting...' + availabilityId);
        Database.delete(availabilityId);
    }
    
}