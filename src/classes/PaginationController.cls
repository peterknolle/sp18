public class PaginationController {

    @AuraEnabled
    public static Info getItemInfo(Integer offsetVal, Integer limitVal) {
        Id userId = UserInfo.getUserId();
        Integer offsetInt = Integer.valueOf(offsetVal);
        Integer limitInt = Integer.valueOf(limitVal);
        
        System.debug(offsetInt);
        System.debug(limitInt);
        
        System.debug(1);
        
        String soql = 
            ' SELECT Id, Name ' +
            ' FROM Account ' +
            ' WHERE OwnerId = \'' + userId + '\'' +
            ' ORDER BY Name ';
            
        
        System.debug(2);
        
        soql += ' LIMIT ' + limitInt; 
        if (offsetInt > 0) {
            soql += ' OFFSET ' + offsetInt;
        }
        
        System.debug(soql);
        
        
        Info inf = new Info();
        inf.items = Database.query(soql);
        
        inf.numTotalItems = [
            SELECT Count()
            FROM Account
            WHERE OwnerId = :userId
        ];
        System.debug(inf);
        return inf;
    }

    public class Info {
        @AuraEnabled
        public List<Account> items { get; set; }
        
        @AuraEnabled
        public Integer numTotalItems { get; set; }
    }
}