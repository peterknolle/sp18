@isTest
private class PricebookEntryTest {
    
    @isTest
    static void pricebookEntryTest() {
        // Generate standard prices
        List<Product2> products = TestUtils.generateProducts(10);
        Map<Id, Decimal> standardProductPrices = TestUtils.generateProductPrices(products, 500);
        List<PricebookEntry> standardEntries = 
            TestUtils.generatePricebookEntries(standardProductPrices, Test.getStandardPricebookId());
        
        // Generate non standard prices
    //    Pricebook2 nonStandardPricebook = TestUtils.generatePricebook();
    //    Map<Id, Decimal> nonStandardProductPrices = TestUtils.generateProductPrices(products, 400);
    //    List<PricebookEntry> nonStandardEntries = 
    //        TestUtils.generatePricebookEntries(nonStandardProductPrices, nonStandardPricebook.Id);
        
        Test.startTest();
        List<PricebookEntry> stdEntries = [Select Id, Pricebook2.Id, Is_Standard__c, Pricebook2Id From PricebookEntry where Is_Standard__c = true];
        System.debug('stdEntries are what');
        System.debug(stdEntries);
        System.debug('the pricebook is standard boolean');
        System.debug(stdEntries.get(0).Is_Standard__c);
        System.debug('the pricebook is standard boolean');
        Test.stopTest();
        
        // assert something
    }
}