@isTest
public class TestUtils {
    public static List<Product2> generateProducts(Integer numProducts) {
        List<Product2> products = new List<Product2>();
        for (Integer i = 0; i < numProducts; i++) {
        	products.add(new Product2(name='Test' + i));    
        }
        
        insert products;
        return products;
    }
    
    public static Map<Id, Decimal> generateProductPrices(List<Product2> products, Decimal baseAmount) {
        Map<Id, Decimal> productPrices = new Map<Id, Decimal>();
        for (Integer i = 0; i < products.size(); i++) {
            productPrices.put(products.get(0).Id, baseAmount + i);
        }
        return productPrices;
    }
    
    
    public static List<PricebookEntry> generatePricebookEntries(Map<Id, Decimal> productPrices, Id pricebookId) {
        List<PricebookEntry> entries = new List<PricebookEntry>();    
        for (Id productId : productPrices.keySet()) {
            entries.add(new PricebookEntry(
                Product2Id = productId,
                Pricebook2Id = pricebookId,
                UnitPrice = productPrices.get(productId),
                IsActive = true
            ));
        }

        insert entries;
        return entries;
	}
    
    public static Pricebook2 generatePricebook() {
        Pricebook2 pricebook = new Pricebook2(
        	IsActive = true,
            Name = 'Unit Test'
        );
        
        insert pricebook;
        return pricebook;
    }
}