@isTest
private class FileUploadControllerTest {
    @testSetup
    static void testSetup() {
		Account a = new Account(Name='Unit Test');
        insert a;
    }
    
    @isTest
    static void testSaveTheFile() {
        Account a = [Select Id From Account Where Name = 'Unit Test'][0];
        
        Test.startTest();
        Id retId = FileUploadController.saveTheFile(a.Id, 'file.txt', 'c3VyZS4=', 'text/plain');
        Test.stopTest();
        
        // Id was returned
        System.assertNotEquals(null, retId, 'Attachment Id should have been created and returned');
        
        // File was created
        List<Attachment> attachments = [Select Id From Attachment Where Id = :retId];
        System.assert(!attachments.isEmpty(), 'attachment not created');
       	
    }
        
    @isTest
    static void testSaveTheChunkExistingFile() {
        Account a = [Select Id From Account Where Name = 'Unit Test'][0];
        
        // Get a file started
        Id attachId = FileUploadController.saveTheFile(a.Id, 'file.txt', 'c3VyZS4=', 'text/plain');
        
        Test.startTest();
        Id retId = FileUploadController.saveTheChunk(a.Id, 'file.txt', 'c3VyZS4=', 'text/plain', attachId);
        Test.stopTest();
        
        // Id was returned
        System.assertNotEquals(null, retId, 'Attachment Id should have been created and returned');
        
        // File was created
        List<Attachment> attachments = [Select Id From Attachment Where Id = :retId];
        System.assert(!attachments.isEmpty(), 'attachment not created');
       	
    }
    
    @isTest
    static void testSaveTheChunkNewFile() {
        Account a = [Select Id From Account Where Name = 'Unit Test'][0];
        
        Test.startTest();
        Id retId = FileUploadController.saveTheChunk(a.Id, 'file.txt', 'c3VyZS4=', 'text/plain', '');
        Test.stopTest();
        
        // Id was returned
        System.assertNotEquals(null, retId, 'Attachment Id should have been created and returned');
        
        // File was created
        List<Attachment> attachments = [Select Id From Attachment Where Id = :retId];
        System.assert(!attachments.isEmpty(), 'attachment not created');
       	
    }
}