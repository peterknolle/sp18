public class GraphSearcher {
    private final Graph g;
        
    private Graph.Node destNode;
    private List<GraphPath> paths;
        
    public GraphSearcher(Graph gr) {
        g = gr;
    }
     
    public List<GraphPath> findPaths(Graph.Node sourceNode, Graph.Node destNode) {
        this.destNode = destNode;
        this.paths = new List<GraphPath>();
          
        GraphPath currentPath = new GraphPath();
            
        search(sourceNode, currentPath);
            
        return paths;
    }
        
    private void search(Graph.Node currentNode, GraphPath currentPath) {
        currentPath.add(currentNode);
        Set<Graph.Node> adjacentNodes = g.getAdjacentNodes(currentNode);
            
        // Check if the adjGraph.Node is the destGraph.Node. 
        // If so, a path has been found
        for (Graph.Node adjNode : adjacentNodes) {
            if (adjNode.equals(destNode)) {
                currentPath.add(adjNode);
                paths.add(new GraphPath(currentPath));
                 
                // remove the destGraph.Node in case there is another
                // way to get to the destGraph.Node
                currentPath.removeLast();
                    
                break;
            } 
        }
            
        // Search the unvisited adjacent Graph.Nodes
        for (Graph.Node adjNode : adjacentNodes) {
            if (!currentPath.contains(adjNode) && !adjNode.equals(destNode)) {
                search(adjNode, currentPath);
                currentPath.removeLast();
            }
        }
            
    }
}