public with sharing class AccountController 
{
    @AuraEnabled
    public static Account getAccount(String id) 
    {
        return [SELECT Id 
                FROM Account 
                WHEre Id = :id];
    }
}