public class CaseController {

    @AuraEnabled
    public static String getOppInfos(String stageName) {
        return '';
    }

    @AuraEnabled
    public static Info getItemInfo(Integer offsetVal, Integer limitVal) {
        Id userId = UserInfo.getUserId();
        Integer offsetInt = Integer.valueOf(offsetVal);
        Integer limitInt = Integer.valueOf(limitVal);
        
        System.debug(offsetInt);
        System.debug(limitInt);
        
        System.debug(1);
        
        String soql = 
            ' SELECT Id, CaseNumber, Account.Name, Account.Id, Contact.Phone, Contact.Email, Contact.Name ' +
            ' FROM Case ' +
            ' WHERE OwnerId = \'' + userId + '\'' +
            ' ORDER BY CaseNumber ';
            
        
        System.debug(2);
        
        soql += ' LIMIT ' + limitInt; 
        if (offsetInt > 0) {
            soql += ' OFFSET ' + offsetInt;
        }
        
        System.debug(soql);
        
        
        Info inf = new Info();
        inf.items = Database.query(soql);
        
        inf.numTotalItems = [
            SELECT Count()
            FROM Case
            WHERE OwnerId = :userId
        ];
        System.debug(inf);
        return inf;
    }

    public class Info {
        @AuraEnabled
        public List<Case> items { get; set; }
        
        @AuraEnabled
        public Integer numTotalItems { get; set; }
    }
    
    class MyException extends Exception {}
}