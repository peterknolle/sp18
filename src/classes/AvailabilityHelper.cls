public with sharing class AvailabilityHelper {
    
    private static final String FIELDS_SOQL = 
        'SELECT ' +
        '   Id, ' + 
        '    Type__c, ' + 
        '    Day__c, ' + 
        '    Start_Time__c, ' + 
        '    End_Time__c, ' + 
        '    Time_Zone__c, ' +
        '    Start_Time_GMT__c, ' +
        '    End_Time_GMT__c ';
    
    private static final String FROM_SOQL = 
        ' FROM Availability__c ';
    
    private static final String ORDER_BY_SOQL = 
        ' ORDER BY Start_Time_GMT__c, End_Time_GMT__c DESC, Type__c, CreatedDate ';
    
    private DateHelper dateHelp = new DateHelper();
    
    public void setGmtTimes(List<Availability__c> availabilites) {
        for (Availability__c a : availabilites) {
            if (a.Start_Time__c == null || a.End_Time__c == null || a.Day__c == null || a.Time_Zone__c == null) {
                continue;
            }
            
            a.Start_Time__c = dateHelp.setDayOfWeek(a.Start_Time__c, a.Day__c);
            a.End_Time__c = dateHelp.setDayOfWeek(a.End_Time__c, a.Day__c);
            
            a.Start_Time_GMT__c = dateHelp.getKnownGmtDateWith(a.Start_Time__c, a.Time_Zone__c);
            a.End_Time_GMT__c = dateHelp.getKnownGmtDateWith(a.End_Time__c, a.Time_Zone__c);
            
            // This situation can occur on borders of the KNOWN DAYs, e.g.,
            // Input of Sunday (7) begin and Sunday (7) end translates to
            // Sunday (7) begin GMT and Monday (1) end GMT.
            if (a.Start_Time_GMT__c > a.End_Time_GMT__c) {
                a.End_Time_GMT__c = a.End_Time_GMT__c.addDays(7);
            }
        }
    }
    
    public Map<Id, Availability__c> getAvailabilities(Set<Id> availabilityIds) {
        Map<Id, Availability__c> availabilities = new Map<Id, Availability__c>();

        for (Availability__c a : [
            SELECT 
            	Id, 
            	Day__c, 
            	Start_Time__c, 
            	End_Time__c, 
            	Start_Time_GMT__c, 
            	End_Time_GMT__c,
            	Type__c, 
            	Time_Zone__c
            FROM Availability__c
            WHERE Id = :availabilityIds
        ]) {
            availabilities.put(a.Id, a);
        }
        
        return availabilities;
    }

    public Availability__c getDefaultAvailability(Id parentId, String sObjectName) {
        Datetime userDateTime = new DateHelper().getUserDatetimeHour(System.now());
        List<Option> typeOptions = new ObjectHelper().getOptions(Availability__c.Type__c);
        
        Availability__c def = new Availability__c();
        def.Day__c = userDateTime.format('EEEE');
        def.Start_Time__c = userDateTime.addHours(1);
        def.End_Time__c = userDateTime.addHours(2);
        def.Time_Zone__c = UserInfo.getTimeZone().getID();
        def.Type__c = !typeOptions.isEmpty() ? typeOptions.get(0).value : null;
        
        Map<String, String> objToField = new ObjectHelper().getFieldNames(new List<String>{sObjectName});
        def.put(objToField.get(sObjectName), parentId);
        
        return def;
    }

    public Map<Id, List<AvailabilityInfo>> getAvailabilities(
        Map<Id, String> parentIdToObjectName, 
        Datetime targetDateTimeGmtBefore
    ) {
        Datetime targetTimeGmt = dateHelp.getKnownDateGmtWith(targetDateTimeGmtBefore);
        Datetime targetPlus7 = dateHelp.getKnownDateGmtPlus7(targetTimeGmt);
        Boolean isMonday = targetPlus7 != null;
        
        Map<Id, List<AvailabilityInfo>> infosBeforeTarget = new Map<Id, List<AvailabilityInfo>>();
        Map<Id, List<AvailabilityInfo>> infosIncludingTarget = new Map<Id, List<AvailabilityInfo>>();
        Map<Id, List<AvailabilityInfo>> infosAfterTarget = new Map<Id, List<AvailabilityInfo>>();

        // parentIds variable name is used in the build up in getSoql
        Set<Id> parentIds = parentIdToObjectName.keySet();
        String soql = getSoql(parentIdToObjectName);
        
        for (Availability__c a : Database.query(soql)) {
            
            Boolean isAvailable = isAvailable(a, targetTimeGmt) ||
                ( isMonday && isAvailable(a, targetPlus7) );
            
            AvailabilityInfo info = new AvailabilityInfo(a, isAvailable);
            
            Id parentId = getParentId(a, parentIdToObjectName);
            
            if (isAvailable) {
                addToInfoList(info, parentId, infosIncludingTarget);
            } 
            else if ( isBefore(a, targetTimeGmt) ||
                     (isMonday && isBefore(a, targetPlus7))  ) {
            	addToInfoList(info, parentId, infosBeforeTarget);
            } 
            else {
                addToInfoList(info, parentId, infosAfterTarget);
            }
        }
        
        return getCombinedInfoList(
            parentIdToObjectName.keySet(), 
            infosBeforeTarget, 
            infosIncludingTarget, 
            infosAfterTarget);
    }
        
    private String getSoql(Map<Id, String> parentIdToObjectName) {
        Set<Id> parentIds = parentIdToObjectName.keySet();
        List<String> parentNames = parentIdToObjectName.values();
        Map<String, String> objNameToFieldName = new ObjectHelper().getFieldNames(parentNames);
        
    	String parentWhere = ' WHERE ';
        String fields = FIELDS_SOQL;
        
        for (Integer i = 0, cnt = parentNames.size(); i < cnt; i++) {
            String parentName = parentNames.get(i);
            String fieldName = objNameToFieldName.get(parentName);
        	fields += ', ' + fieldName; 
            parentWhere += ' ' + fieldName + ' IN :parentIds ';
            if (i < (cnt - 1)) {
                parentWhere += ' OR ';
            }
        }
        
        String soql = 
            fields + 
            FROM_SOQL +
            parentWhere +
            ORDER_BY_SOQL;

        return soql;
    }
    
    private Id getParentId(Availability__c a, Map<Id, String> parentIdToObjectName) {
  		List<String> parentNames = parentIdToObjectName.values();
        Map<String, String> objNameToFieldName = new ObjectHelper().getFieldNames(parentNames);
        for (String parentName : parentNames) {
            Id parentId = (Id) a.get(objNameToFieldName.get(parentName));
            if (parentId != null && parentIdToObjectName.get(parentId) != null) {
                return parentId;
            }
        }
        return null;
    }
    
    private Boolean isAvailable(Availability__c a, Datetime target) {
        return a.Start_Time_GMT__c <= target && target <= a.End_Time_GMT__c;
    }
    
    private Boolean isBefore(Availability__c a, Datetime target) {
        return a.Start_Time_GMT__c < target;
    }
    
    private void addToInfoList(AvailabilityInfo info, Id parentId, Map<Id, List<AvailabilityInfo>> infos) {
        List<AvailabilityInfo> childInfos = infos.get(parentId);
        if (childInfos == null) {
            childInfos = new List<AvailabilityInfo>();
            infos.put(parentId, childInfos);
        }
        
        childInfos.add(info);
    }
    
    private Map<Id, List<AvailabilityInfo>> getCombinedInfoList(
        Set<Id> keys, 
        Map<Id, List<AvailabilityInfo>> infosBeforeTarget,
        Map<Id, List<AvailabilityInfo>> infosIncludingTarget,
        Map<Id, List<AvailabilityInfo>> infosAfterTarget
    ) {
        Map<Id, List<AvailabilityInfo>> allInfos = new Map<Id, List<AvailabilityInfo>>();
        
        for (Id key : keys) {
            
            // Note the order here is to first add those that are currently available,
            // so that they show up first...
            // then AFTER for what's available next...
            // then finally BEFORE as they are furthest away.
            List<AvailabilityInfo> including = infosIncludingTarget.get(key);
            addToInfoList(including, key, allInfos);
            
            List<AvailabilityInfo> after = infosAfterTarget.get(key);
            addToInfoList(after, key, allInfos);
            
            List<AvailabilityInfo> before = infosBeforeTarget.get(key);
            addToInfoList(before, key, allInfos);
        }
        
        return allInfos;
    }
    
    private void addToInfoList(List<AvailabilityInfo> additions, Id key, Map<Id, List<AvailabilityInfo>> allInfos) {
        if (additions == null) {
            return;
        }
        
        List<AvailabilityInfo> infos = allInfos.get(key);
        if (infos == null) {
            infos = new List<AvailabilityInfo>();
            allInfos.put(key, infos);
        }
        
        infos.addAll(additions);
    } 
}