public with sharing class DateHelper {

    private static Map<String, Integer> DAY_INDEXES;
    private static Map<String, Datetime> KNOWN_DAY_DATETIMES_GMT;
    
    public Map<String, Integer> getDayIndexes() {
        if (DAY_INDEXES == null) {
            DAY_INDEXES = new Map<String, Integer>{
                'Monday'    => 1,
                'Tuesday'   => 2,
                'Wednesday' => 3,
                'Thursday'  => 4,
                'Friday'    => 5,
                'Saturday'  => 6,
                'Sunday'    => 7
        	};
        }
        
        return DAY_INDEXES;
    }
    
    private static Map<String, Datetime> getKnownDateTimesGmt() {
        if (KNOWN_DAY_DATETIMES_GMT == null) {
            KNOWN_DAY_DATETIMES_GMT = new Map<String, DateTime>{
                'Monday'    => Datetime.newInstanceGmt(2016, 1, 4, 0, 0, 0),
                'Tuesday'   => Datetime.newInstanceGmt(2016, 1, 5, 0, 0, 0),
                'Wednesday' => Datetime.newInstanceGmt(2016, 1, 6, 0, 0, 0),
                'Thursday'  => Datetime.newInstanceGmt(2016, 1, 7, 0, 0, 0),
                'Friday'    => Datetime.newInstanceGmt(2016, 1, 8, 0, 0, 0),
                'Saturday'  => Datetime.newInstanceGmt(2016, 1, 9, 0, 0, 0),
                'Sunday'    => Datetime.newInstanceGmt(2016, 1, 10, 0, 0, 0)
        	};
        }
        
        return KNOWN_DAY_DATETIMES_GMT;
    }
        
    public Datetime getKnownDayDateTimeGmt(String day) {
        return getKnownDateTimesGmt().get(day);
    }
    
    public Datetime getKnownDayDateTimeGmt(Datetime targetDateTime) {
        String day = targetDateTime.formatGmt('EEEE');
        return getKnownDateTimesGmt().get(day);
    }
    
    public DateTime getKnownGmtDateWith(Datetime timeLocal, String timezoneKey) {
        Datetime adjustedTimeLocal = getGmtTimeWith(timeLocal, timezoneKey);
        Datetime knownDateGmtWithAdjustedTime = getKnownDateGmtWith(adjustedTimeLocal);
        
        return knownDateGmtWithAdjustedTime;
    }

    public Datetime getGmtTimeWith(Datetime timeLocal, String timeZoneKey) {
        // Since time is confusing, an embedded example is included in comments
        // An America/Los_Angles User input: 7:30 PM, Saturday, and America/New_York
        
       	// timeLocal = 1/16/2016 Sat 7:30 PM (America/Los_Angeles is User's)
       	// gmtLocal =  1/17/2016 Sun 3:30 AM
       	// timeZoneKey = America/New_York (+3 of the User's)
       	
        // Get timeLocal to equal inputted 7:30 PM America/New_York, (4:30 LA)
        // adjustedTimeLocal = 1/16/2016 Sat 4:30 PM America/Los_Angeles
        Datetime adjustedTimeLocal = addTimeZoneDifference(
            timeLocal, 
            UserInfo.getTimeZone(), 
            System.TimeZone.getTimeZone(timeZoneKey));
        
        // adjustedTimeGmt = 1/17/2016 Sun 12:30 AM
        DateTime adjustedTimeGmt = Datetime.newInstanceGmt(
        	adjustedTimeLocal.dateGmt(),
            adjustedTimeLocal.timeGmt());
        
        return adjustedTimeGmt;
    }
    
    public Datetime getKnownDateGmtWith(Datetime timePart) {
        // theDate = 1/17/2016 Sun 12:30 AM
        
        // At this point, change to the known date, 
        // since the day of the week is correct for the gmt time
      	// knownDateTimeGmt, a constant, = 1/10/2016 12:00 AM
        Datetime knownDateTimeGmt = getKnownDayDateTimeGmt(timePart);
        
        // knownDateWithInputGmtTime = 1/10/2016 12:30 AM
        DateTime knownDateWithInputGmtTime = Datetime.newInstanceGmt(
            knownDateTimeGmt.dateGmt(),
            timePart.timeGmt());
        
        return knownDateWithInputGmtTime;
    }
    
    public Datetime addTimeZoneDifference(Datetime sourceTzTime, TimeZone sourceTz, TimeZone destTz) {
        Integer sourceTzOffset = sourceTz.getOffset(sourceTzTime);
        Integer destTzOffset = destTz.getOffset(sourceTzTime);
        Integer millisToAdd = sourceTzOffset - destTzOffset;
        
        Datetime adjustedTime = sourceTzTime.addMinutes(millisToAdd / (1000 * 60));
        
        return adjustedTIme;
    }
    
    // The "plus7" will only return a value when the date is a Monday
    // Returns null for all non Mondays.
    public DateTime getKnownDateGmtPlus7(Datetime gmtDateTime) {
        Boolean isMonday = gmtDateTime.format('u') == '1';
        
		Datetime gmtDateTimePlus7 = null;
        if (isMonday) {
            gmtDateTimePlus7 = gmtDateTime.addDays(7);
        }
        
        return gmtDateTimePlus7;
    }
    
    public Datetime getUserDatetimeHour(Datetime gmtDatetime) {
        Time gmtTime = Time.newInstance(gmtDatetime.time().hour(), 0, 0, 0);
        Datetime userDateTime = Datetime.newinstance(gmtDatetime.date(), gmtTime); 
        
        return userDateTime;
    }
    
    public Datetime setDayOfWeek(Datetime dt, String dayOfWeek) {
        // Monday is always 1
        Integer dtIdx = Integer.valueOf( dt.format('u') );
       	Integer dayOfWeekIdx = getDayIndexes().get(dayOfWeek);
        
        Integer daysToAdd = dayOfWeekIdx - dtIdx;
        
        dt = dt.addDays(daysToAdd);
        return dt;
    }
}