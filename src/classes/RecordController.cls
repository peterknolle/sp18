public class RecordController {
 
    @AuraEnabled
    public static sObject getObject(Id recordId, String objectType, String fields) {
    System.debug('fields='+fields);
        Set<String> fieldSet = new Set<String>(fields.split(','));
        
        String soql = 
            ' SELECT ' + String.join(new List<String>(fieldSet), ',') +
            ' FROM ' + objectType + 
            ' WHERE Id = :recordId';

        List<sObject> objs = Database.query(soql);
        if (!objs.isEmpty()) {
            return objs.get(0);
        }
        
        return null;
    }
}