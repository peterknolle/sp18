public with sharing class AvailabilityManagerController {
    private static AvailabilityHelper availabilityHelp = new AvailabilityHelper();
    
    @AuraEnabled
    public static List<AvailabilityInfo> getAvailabilities(Id recordId, String sObjectName) {
        Datetime now = Datetime.now();
        Map<Id, List<AvailabilityInfo>> avails = availabilityHelp.getAvailabilities(
            new Map<Id, String>{ recordId => sObjectName },
            now);
        
        List<AvailabilityInfo> infos =  avails.get(recordId);
        return infos != null ? infos : new List<AvailabilityInfo>();
    }

    @AuraEnabled
    public static Availability__c getAvailability(Id availabilityId) {
        Map<Id, Availability__c> avails = availabilityHelp.getAvailabilities(
            new Set<Id>{availabilityId});
        
        return avails.get(availabilityId);
    }
    
    @AuraEnabled
    public static Availability__c getDefaultAvailability(Id recordId, String sObjectName) {
        // TODO: bulkify
        Availability__c def = availabilityHelp.getDefaultAvailability(recordId, sObjectName);
        return def;
    }
}