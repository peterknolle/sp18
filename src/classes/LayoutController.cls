public with sharing class LayoutController {

    @AuraEnabled
    public static ByMethodA initialize(Id myObjId) {
        ByMethodA bma = new ByMethodA();
        bma.currentUser = getCurrentUser();
        bma.availableUsers = getAvailableUsers();
        bma.availableAccounts = getAvailableAccounts();
        bma.myObject = getMyObject(myObjId);
        if (bma.myObject.User__c == null) {
            bma.myObject.User__c = bma.currentUser.Id;
            bma.myObject.User__r = bma.currentUser;
        }

        return bma;
    }

    @AuraEnabled
    public static List<User> getAvailableUsers() {
        return [ SELECT Id, Name FROM User Order By Name ];
    }

    @AuraEnabled
    public static List<Contact> getAvailableContacts(Id accountId) {
        return [ SELECT Id, Name FROM Contact WHERE AccountId = :accountId ];
    }

    @AuraEnabled
    public static List<Opportunity> getAvailableOpportunites(Id contactId) {
        return [ SELECT Id, Name FROM Opportunity WHERE AccountId IN (SELECT AccountId FROM Contact WHERE Id = :contactId) ORDER BY Name];
    }

    private static User getCurrentUser() {
        return [ SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId() ];
    }

    private static List<Account> getAvailableAccounts() {
        return [ SELECT Id, Name FROM Account ORDER BY Name ];
    }

    private static My_Object__c getMyObject(Id myObjId) {
        List<My_Object__c> myObjSoql = [
            SELECT Id, Name, Account__c, Contact__c, Opportunity__c, User__c
            FROM My_Object__c
            WHERE Id = :myObjId
        ];
        return myObjSoql.isEmpty() ? new My_Object__c() : myObjSoql[0];
    }

    public class ByMethodA {
        @AuraEnabled public User currentUser;

        @AuraEnabled public List<User> availableUsers;
        @AuraEnabled public List<Account> availableAccounts;
        @AuraEnabled public List<Contact> availableContacts; // dependent on account
        @AuraEnabled public List<Opportunity> availableOpportunities; // dependent on contact

        @AuraEnabled public My_Object__c myObject; // where the data is tracked
    }
}
