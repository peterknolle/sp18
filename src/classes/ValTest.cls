@isTest
private class ValTest {

    @isTest
    static void testConstructionState() {
        Test.startTest();
        Val v = new Val();
        Test.stopTest();
        
        System.assertEquals(null, v.x);
        System.assertEquals(null, v.y);
    }
}