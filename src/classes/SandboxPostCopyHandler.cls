global class SandboxPostCopyHandler implements SandboxPostCopy {
  global void runApexClass(SandboxContext context) {
    System.debug('doIt' + context.organizationId() +
                 context.sandboxId() + context.sandboxName());
  }
}