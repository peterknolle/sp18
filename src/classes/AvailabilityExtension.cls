public with sharing class AvailabilityExtension {
    public Id parentId { get; set; }
    public Availability__c availability { get; set; }

    public AvailabilityExtension(ApexPages.StandardController controller) {
        parentId = controller.getId();
    }
  
}