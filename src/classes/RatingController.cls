public class RatingController {

    @AuraEnabled
    public static Integer loadRating(String recordId) {
        List<Account> accts = [
            SELECT Rating__c
            FROM Account
            WHERE Id = :recordId
        ];
        
        Integer val = (Integer) (!accts.isEmpty() ? accts.get(0).Rating__c : 0.0);
        return val;
    }
    
    @AuraEnabled
    public static void saveRating(String recordId, Decimal rating) {
        Account a = new Account(Id = recordId, Rating__c = rating);
        update a;
    }
}